package main

import (
	"html/template"

	"./HandleSession"
	"gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
	"gopkg.in/kataras/iris.v6/adaptors/view"

	"fmt"

	"gopkg.in/kataras/iris.v6/adaptors/sessions"
)

func main() {
	app := iris.New()
	app.Adapt(httprouter.New())
	sesiones := sessions.New(sessions.Config{Cookie: "hardcookie"})
	app.Adapt(sesiones)

	app.Adapt(view.HTML("./Vistas", ".html").Reload(true))
	app.Set(iris.OptionCharset("UTF-8"))

	app.StaticWeb("/css", "./Recursos/css")
	app.StaticWeb("/scss", "./Recursos/scss")
	app.StaticWeb("/js", "./Recursos/js")
	app.StaticWeb("/img", "./Recursos/img")

	app.Get("/", Acceso)
	app.Post("/", Login)
	app.Get("/Index", IndexFn)
	app.Get("/Logout", Logout)
	app.Get("/Sistema", Sistema)
	app.Get("/SesionesActivas", SesionesActivas)

	app.Get("/Sistema/Sesiones/VerByName", SesionesByNameGet)
	app.Post("/Sistema/Sesiones/VerByName", SesionesByNamePost)

	app.Get("/Sistema/Sesiones/EliminarByName", EliminaByNameGet)
	app.Post("/Sistema/Sesiones/EliminarByName", EliminaByNamePost)

	app.Post("/Sistema/Sesiones/CerrarByID", CerrarSesionByID)

	fmt.Println("Listen in: localhost:8080")
	app.Listen(":8080")

}

//Acceso carga la vista principal
func Acceso(ctx *iris.Context) {
	data := ctx.Session().Get("data")
	if data != nil {
		ctx.Redirect("Index", iris.StatusFound)
	}
	ctx.Render("login.html", nil)
}

//IndexFn carga la vista principal
func IndexFn(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session

	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}
	ctx.Render("Inicio.html", Datos)
}

//Login carga la vista principal
func Login(ctx *iris.Context) {
	usuario := ctx.FormValue("Usuario")
	if usuario != "" {
		ses := HandleSession.CreateSession(usuario, ctx)
		if ses != nil { //Crea una session con los parametros pasados
			ctx.Writef("%s", ses)
		} else {
			ctx.Redirect("/Index", iris.StatusFound)
		}
	} else {
		ctx.Redirect("/", iris.StatusFound)
	}

}

//Logout carga la vista principal
func Logout(ctx *iris.Context) {
	ses := HandleSession.DestroySession(ctx)
	if ses != nil { //Crea una session con los parametros pasados
		ctx.Writef("%s", ses)
	}
}

//Sistema carga la vista principal
func Sistema(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session

	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}
	ctx.Render("Sistema.html", Datos)
}

// CerrarSesionByID cierra la sesion del id rcibido
func CerrarSesionByID(ctx *iris.Context) {
	IDSes := ctx.FormValue("IDSesion")
	if IDSes != "" {
		err := HandleSession.EliminaSesionByID(IDSes)
		if err != nil {
			ctx.Writef("Error al Eliminar Sesion %s Error: %s", IDSes, err)
		}
		ctx.Redirect("/SesionesActivas", iris.StatusFound)
	}
}

// SesionesActivas verifica todas las sesiones activas del sistema
func SesionesActivas(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session
	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}
	sesiones, err := HandleSession.RegresaSesionesActivas()
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}

	Datos.Tabla = template.HTML(CreaTablaSesiones(sesiones))

	ctx.Render("SesionesActivas.html", Datos)

}

// SesionesByNameGet carga la vista de SesionesActivasUsuario
func SesionesByNameGet(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session

	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}
	ctx.Render("SesionesActivasUsuarioVer.html", Datos)

}

// SesionesByNamePost muestra las sesiones activas de un usuario
func SesionesByNamePost(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session
	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}

	usuario := ctx.FormValue("username")

	sesiones, err := HandleSession.GetInfoSession(usuario)
	if err != nil {
		ctx.Writef("Ocurrio un problema al cerrar Sesiones de %s EROR: %s", usuario, err)
		return
	}
	Datos.Tabla = template.HTML(CreaTablaSesiones(sesiones))

	ctx.Render("SesionesActivasUsuarioVer.html", Datos)

}

// EliminaByNameGet carga la vista de SesionesActivasUsuario
func EliminaByNameGet(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session

	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}
	ctx.Render("SesionesActivasUsuarioDel.html", Datos)

}

//EliminaByNamePost elimina las sesiones Activas de un usuario
func EliminaByNamePost(ctx *iris.Context) {
	Datos := Sesion{}
	nombre, _, _, err := HandleSession.GetDataSession(ctx) //Retorna los datos de la session

	Datos.Nombre = nombre
	if err != nil {
		ctx.Writef("Error: %s ", err)
		return
	}

	usuario := ctx.FormValue("username")
	err = HandleSession.EliminarSesionesUsr(usuario)
	if err != nil {
		ctx.Writef("Ocurrio un problema al cerrar Sesiones de %s EROR: %s", usuario, err)
		return
	}
	fmt.Println(usuario)
	ctx.Render("SesionesActivasUsuarioDel.html", Datos)
}

func CreaTablaSesiones(sesiones []map[string]string) string {
	html := ""
	html += `<table class="table">
				<thead class="thead-inverse">
				<tr>
				<th>ID</th>
				<th>UserAgent</th>
				<th>Ubicacion Actual</th>
				<th>IP</th>
				<th>ServerHost</th>
				<th>Fecha</th>
				<th>VirtualHostName</th>
				</tr>
			</thead>
			<tbody>`

	for _, val := range sesiones {
		html += `<tr>`
		html += `<td>` + val["IDSession"] + `</td>`
		html += `<td>` + val["UserAgent"] + `</td>`
		html += `<td>` + val["Ubicacion"] + `</td>`
		html += `<td>` + val["IP"] + `</td>`
		html += `<td>` + val["ServerHost"] + `</td>`
		html += `<td>` + val["Time"] + `</td>`
		html += `<td>` + val["VirtualHostName"] + `</td>`
		html += `</tr>`
	}
	html += `</tbody>
			</table>`
	return html
}

//Sesion datos de sesion
type Sesion struct {
	Nombre string
	Tabla  template.HTML
}
